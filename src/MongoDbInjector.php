<?php

namespace svk\MongoDbInjector;

class MongoDbInjector {
    public function __construct() {
        if (!extension_loaded('mongodb')) {
            dl('mongodb.'.phpversion().'.so');
        }
    }
}
